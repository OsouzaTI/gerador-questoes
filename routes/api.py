from flask import Blueprint
from constants import idade


api_blueprint = Blueprint('api', __name__)

@api_blueprint.route('/')
def api():
    return {
        'teste': idade.tolist()
    }