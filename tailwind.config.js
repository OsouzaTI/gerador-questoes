/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./templates/**/*.html"],
  theme: {
    extend: {
      colors: {
        dark: {
          fundo: '#222222',
          texto: '#FFF',
          card: {
            fundo: '#4e4e4e',
            header: '#646464'
          },
          pagina: '#3e3e3e',
          botao: '#151515'
        }
      },
    },
  },
  plugins: [],
}

