import numpy as np

import skfuzzy as fuzz
from skfuzzy import control as ctrl

# Variaveis da lógica de fuzzy

VOCABULARIO     = ['SIMPLES', 'MEDIO', 'DIFICIL']
IDADES          = ['CRIANCA',  'ADOLESCENTE',  'ADULTO']

niveis = {
    'contexto':     np.arange(0, 11, 1),
    'idade':        np.arange(12, 61, 1),
    'formalidade':  np.arange(0, 11, 1)
}

# Criando as variáveis de Antecedente e Consequente
contexto    = ctrl.Antecedent(niveis['contexto'], 'contexto')
idade       = ctrl.Antecedent(niveis['idade'],  'idade')
formalidade = ctrl.Consequent(niveis['formalidade'], 'formalidade')

# Auto-membership functions
contexto.automf(3, names=VOCABULARIO)
idade.automf(3, names=IDADES)

# Funções de pertinência para formalidade
formalidade['MUITO SIMPLES']    = fuzz.trimf(formalidade.universe, [0, 2, 3])
formalidade['SIMPLES']          = fuzz.trimf(formalidade.universe, [3, 4, 5])
formalidade['MEDIO']            = fuzz.trimf(formalidade.universe, [4, 8, 7])
formalidade['DIFICIL']          = fuzz.trimf(formalidade.universe, [8, 10, 10])

# Regras Difusas
rule1 = ctrl.Rule(contexto['SIMPLES'] & idade['CRIANCA'], formalidade['MUITO SIMPLES'])
rule2 = ctrl.Rule(contexto['SIMPLES'] & idade['ADOLESCENTE'], formalidade['SIMPLES'])
rule3 = ctrl.Rule(contexto['MEDIO']   & idade['ADOLESCENTE'], formalidade['MEDIO'])
rule4 = ctrl.Rule(contexto['MEDIO']   & idade['ADULTO'], formalidade['MEDIO'])
rule5 = ctrl.Rule(contexto['DIFICIL'], formalidade['DIFICIL'])

# Sistema de controle e simulação
texto_ctrl = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5])
texto = ctrl.ControlSystemSimulation(texto_ctrl)

# Passando entradas para o sistema de controle
texto.input['contexto'] = 8  # Mais para o formal
texto.input['idade'] = 25   # Adulto

# Computação
texto.compute()
nivel_formalidade = texto.output['formalidade']
print(f"Nível de formalidade do texto: {nivel_formalidade}")  # Mostra o gráfico de pertinência